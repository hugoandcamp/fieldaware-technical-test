# FieldAware Technical Test #

### Installation and Test Execution (Mac/Linux/Windows) ###

* Download the project from Bitbucket [fieldaware-technical-test](https://bitbucket.org/hugoandcamp/fieldaware-technical-test)
* Install the latest version of Mozzila Firefox
* Install Ruby 2.2.3 or higher
* Install Ruby Gems running this line using your command-line
```
#!
gem install rake bundler yard watir-webdriver
```
* Run the tests navigating to the project root folder and run this line on your command line

```
#!

cucumber /features/navigation_bar.feature
```

### In your opinion, using a percentage value, how complete is your prototype judged by your own standards ( e.g. "The tests are 75% complete, as I did not do XYZ..." ) ###
   I think this prototype covers a bit less than 50% of the possible tests. I would include the other sub-menus to the created scenarios and create other scenarios to test all the links/buttons and ensure that they redirect the user to the proper pages.

### What improvements, if given more time, would you make to your prototype? ( e.g. "Given a day to work on this further, I would add feature X because..." ) ###
   With more time I would improve the tests more abstract and if not succeeded move all the test data into a separate file.

### What areas, if any, proved difficult, and why? ###
   There was no specific difficulties to develop this test scenarios.

###  ###
*If you any question you can reach me under the email [hugoandcamp@gmail.com](mailto:hugoandcamp@gmail.com).*