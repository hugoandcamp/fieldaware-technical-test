require 'watir-webdriver'

Before do
  @browser = Watir::Browser.new :firefox
  visit(HomePage)
end

After do
  @browser.close
end