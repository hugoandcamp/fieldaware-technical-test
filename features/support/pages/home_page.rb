class HomePage
  include PageObject

  MENU_ITEMS = ['Product', 'Industry', 'Solutions', 'Clients', 'Resources', 'Company', 'Get a Demo']
  PRODUCT_SUB_MENU_LINKS = ['Why FieldAware', 'Asset Management', 'Business Intelligence', 'Mobile Applications',
                            'Scheduling and Dispatch', 'Work Order Management', 'Customer Portal', 'Barcode Scanner',
                            'Improved Cash Flow', 'Customer Satisfaction', 'Service & Worker Productivity',
                            'ERP Integration', 'CRM Integration', 'Licensing Options', 'Implementation Services']


  page_url "http://www.fieldaware.com"

  unordered_list(:menu_items, class: 'nav')
  links(:sub_menu_items, xpath: './/li[contains(@class,"open")]//a[contains(@href,"/")]')
  button(:get_a_demo, class: 'btn-demo')

  def menu_item_click(menu_item)
    menu_items_element[get_menu_item_index(menu_item)].click
  end

  def has_menu_items?
    menu_items_element.items - 1 == MENU_ITEMS.size
  end

  def has_select_sub_menu_items?
    sub_menu_items_elements.size == PRODUCT_SUB_MENU_LINKS.size
  end

  def has_menu_items_display_order?
    MENU_ITEMS.each_with_index.all? { |menu_item, index| menu_item == menu_items_element[index].text }
  end

  def has_sub_menu_items_display_order?
    PRODUCT_SUB_MENU_LINKS.each_with_index.all? { |sub_menu_item, index| sub_menu_item == sub_menu_items_elements[index].text }
  end

  private

  def get_menu_item_index(menu_item)
    MENU_ITEMS.find_index { |m_item| m_item == menu_item}
  end

end