When(/^I click the "([^"]*)" menu item$/) do |menu_item|
  on(HomePage).menu_item_click(menu_item)
end

Then(/^I should see all navigation bar menu items$/) do
  on(HomePage).should have_menu_items
end

Then(/^I should see all sub\-menu items from the select menu$/) do
  on(HomePage).should have_select_sub_menu_items
end

Then(/^I should see all navigation bar menu items in the correct order$/) do
  on(HomePage).should have_menu_items_display_order
end

Then(/^I should see all sub\-menu items from the selected menu in the correct order$/) do
  on(HomePage).should have_sub_menu_items_display_order
end