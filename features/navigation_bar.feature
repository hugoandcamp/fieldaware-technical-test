Feature: Navigation Bar

  @high_priority
  Scenario: Ensure that all navigation bar and Product sub-menu items are displayed
    Then I should see all navigation bar menu items
    When I click the "Product" menu item
    Then I should see all sub-menu items from the select menu

  @normal_priority
  Scenario: Ensure that all navigation bar and Product sub-menu items are displayed in the correct order
    Then I should see all navigation bar menu items in the correct order
    When I click the "Product" menu item
    Then I should see all sub-menu items from the selected menu in the correct order

